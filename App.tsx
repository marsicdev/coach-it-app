import React from 'react'
import { AuthProvider } from './screens/AuthProvider'

import RootNavigation from './screens/RootNavigation'

export default function App() {
    return (
        <AuthProvider>
            <RootNavigation />
        </AuthProvider>
    )
}
