import React, { useContext } from 'react'

import { NavigationContainer } from '@react-navigation/native'
import AuthStackNavigator from './AuthStackNavigator'
import MainStackNavigator from './MainStackNavigator'
import AuthContext, { useAuth } from './AuthContext'

export default function RootNavigation() {
    // const { isLoggedIn } = useContext(AuthContext)
    const { isLoggedIn } = useAuth()
    return (
        <NavigationContainer>
            {isLoggedIn ? <MainStackNavigator /> : <AuthStackNavigator />}
        </NavigationContainer>
    )
}
