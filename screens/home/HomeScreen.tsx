import { StatusBar } from 'expo-status-bar'
import React, { useContext } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

import AuthContext from '../AuthContext'

export default function HomeScreen() {
    const ctx = useContext(AuthContext)

    const handleLogout = () => {
        console.log('Logout')
        ctx.logout()
    }

    return (
        <View style={styles.container}>
            <Text>This is Home</Text>
            <StatusBar style="auto" />
            <Button title="Logout" onPress={handleLogout} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
