import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from './HomeScreen'

const LoginStack = createStackNavigator()

export default function HomeStackNavigator() {
    return (
        <LoginStack.Navigator>
            <LoginStack.Screen name="Evidencije" component={HomeScreen} />
            {/* <LoginStack.Screen name="Evidencije" component={HomeScreen} /> */}
            {/* <LoginStack.Screen name="Register" component={RegisterScreen} /> */}
        </LoginStack.Navigator>
    )
}
