import { StatusBar } from 'expo-status-bar'
import React, { useContext, useState } from 'react'
import {
    ActivityIndicator,
    Button,
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native'

import RNDateTimePicker from '@react-native-community/datetimepicker'
import { playerService } from '../../../services/playerService'

export default function CreatePlayerScreen({ navigation }: any) {
    const [firstName, setFirstName] = React.useState('')
    const [lastName, setLastName] = React.useState('')
    const [isLoading, setLoading] = useState(false)

    const handleCreatePlayer = () => {
        setLoading(true)
        playerService.savePlayer(firstName, lastName).then(() => {
            setLoading(false)
            setFirstName('')
            setLastName('')
            navigation.goBack()
        })
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                placeholder="First name"
                value={firstName}
                onChangeText={setFirstName}
            />
            <TextInput
                style={styles.input}
                placeholder="Last name"
                value={lastName}
                onChangeText={setLastName}
                secureTextEntry
            />
            {isLoading && <ActivityIndicator size="large" />}
            <Button onPress={handleCreatePlayer} title="Create player" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 10,
    },
    input: {
        margin: 10,
        backgroundColor: 'white',
        padding: 10,
        width: '100%',
    },
})
