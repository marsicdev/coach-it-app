import React from 'react'
import { Button } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import PlayersScreen from './PlayersScreen'
import CreatePlayerScreen from './CreatePlayerScreen'

const LoginStack = createStackNavigator()

export default function PlayerStackNavigator({ navigation }: any) {
    const handleOnCreatePlayer = () => {
        navigation.navigate('CreatePlayer')
    }

    return (
        <LoginStack.Navigator>
            <LoginStack.Screen
                name="Players"
                component={PlayersScreen}
                options={{
                    headerRight: () => (
                        <Button title="Add player" onPress={handleOnCreatePlayer} />
                    ),
                }}
            />
            <LoginStack.Screen name="CreatePlayer" component={CreatePlayerScreen} />
        </LoginStack.Navigator>
    )
}
