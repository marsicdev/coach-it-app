import { useFocusEffect } from '@react-navigation/core'
import { StatusBar } from 'expo-status-bar'
import React, { useContext, useEffect, useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { playerService } from '../../../services/playerService'

export default function PlayersScreen() {
    const [player, setPlayer] = useState<any | null>(null)

    useFocusEffect(() => {
        playerService.loadPlayer().then((savedPlayer) => {
            setPlayer(savedPlayer)
        })
    })

    // useEffect(() => {
    //     playerService.loadPlayer().then((savedPlayer) => {
    //         setPlayer(savedPlayer)
    //     })
    // }, [])

    return (
        <View style={styles.container}>
            <Text>This is saved Player</Text>
            {player && (
                <>
                    <Text>{player?.firstName}</Text>
                    <Text>{player?.lastName}</Text>
                </>
            )}
            <StatusBar style="auto" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'violet',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
