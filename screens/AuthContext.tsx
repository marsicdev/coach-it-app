import React, { useContext } from 'react'

const AuthContext = React.createContext({
    isLoggedIn: false,
    login: () => undefined,
    logout: () => undefined,
})

export default AuthContext

export const useAuth = () => {
    return useContext(AuthContext)
}
