import { StatusBar } from 'expo-status-bar'
import React, { useContext } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import AuthContext from '../AuthContext'

export default function LoginScreen() {
    const ctx = useContext(AuthContext)

    const handleLogin = () => {
        console.log('Login')
        ctx.login()
    }

    return (
        <View style={styles.container}>
            <Text>This is LoginScreen</Text>
            <StatusBar style="auto" />
            <Button title="Login" onPress={handleLogin} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
