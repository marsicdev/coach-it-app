import React, { useEffect, useState } from 'react'
import { authService } from '../services/authService'
import AuthContext from './AuthContext'

interface Props {}

export const AuthProvider = (props: any) => {
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false)

    useEffect(() => {
        _bootstrapAsync()
    }, [])

    const _bootstrapAsync = async () => {
        const isAuthenticated = await authService.isLoggedIn()
        console.log(`isAuthenticated`, isAuthenticated)
        setIsLoggedIn(!!isAuthenticated)
    }

    const logout = () => {
        setIsLoggedIn(false)
        return undefined
    }

    const login = () => {
        authService.saveLoggedIn(true).then(() => {
            setIsLoggedIn(true)
        })
        return undefined
    }

    const providerValue = {
        isLoggedIn,
        login,
        logout,
    }

    return (
        <AuthContext.Provider value={providerValue}>
            {props.children}
        </AuthContext.Provider>
    )
}
