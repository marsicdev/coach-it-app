import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import LoginScreen from './auth/LoginScreen'
import RegisterScreen from './auth/RegisterScreen'

const LoginStack = createStackNavigator()

export default function AuthStackNavigator() {
    return (
        <LoginStack.Navigator>
            <LoginStack.Screen name="Login" component={LoginScreen} />
            <LoginStack.Screen name="Register" component={RegisterScreen} />
        </LoginStack.Navigator>
    )
}
