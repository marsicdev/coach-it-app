import React from 'react'

import HomeScreen from './home/HomeScreen'
import { createDrawerNavigator } from '@react-navigation/drawer'
import HomeStackNavigator from './home/HomeStackNavigator'
import PlayerStackNavigator from './home/players/PlayerStackNavigator'

// const MainStack = createStackNavigator()
const Drawer = createDrawerNavigator()

export default function MainStackNavigator() {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Evidencije" component={HomeStackNavigator} />
            <Drawer.Screen name="Igraci" component={PlayerStackNavigator} />
            <Drawer.Screen name="Termini" component={HomeScreen} />
        </Drawer.Navigator>
    )
}
