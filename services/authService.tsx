import AsyncStorage from '@react-native-async-storage/async-storage'

class AuthService {
    saveLoggedIn = async (loggedIn: boolean) => {
        await AsyncStorage.setItem('loggedIn', JSON.stringify(loggedIn))
    }

    isLoggedIn = () => {
        return AsyncStorage.getItem('loggedIn')
    }
}

export const authService = new AuthService()
