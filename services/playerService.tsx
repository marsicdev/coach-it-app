import AsyncStorage from '@react-native-async-storage/async-storage'

class PlayerService {
    playerKey = '@player'

    savePlayer = async (firstName: string, lastName: string) => {
        const player = {
            firstName,
            lastName,
        }

        const playerData = JSON.stringify(player)
        await AsyncStorage.setItem(this.playerKey, playerData)
    }

    loadPlayer = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem(this.playerKey)
            return jsonValue != null ? JSON.parse(jsonValue) : null
        } catch (e) {
            // error reading value
        }
    }
}

export const playerService = new PlayerService()
